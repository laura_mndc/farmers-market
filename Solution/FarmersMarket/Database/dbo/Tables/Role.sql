﻿CREATE TABLE [dbo].[Role] (
    [ID]    INT          IDENTITY (1, 1) NOT NULL,
    [Title] VARCHAR (30) NOT NULL,
    CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED ([ID] ASC)
);

