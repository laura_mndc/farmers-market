﻿CREATE TABLE [dbo].[Invoice] (
    [ID]       INT           IDENTITY (1, 1) NOT NULL,
    [IssuedOn] DATETIME2 (2) NOT NULL,
    [PaidOn]   DATETIME2 (2) NULL,
    [BuyerID]  INT           NOT NULL,
    CONSTRAINT [PK_Invoice] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Invoice_UserId] FOREIGN KEY ([BuyerID]) REFERENCES [dbo].[User] ([ID])
);

