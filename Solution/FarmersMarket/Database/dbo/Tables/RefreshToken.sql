﻿CREATE TABLE [dbo].[RefreshToken] (
    [ID]             INT            IDENTITY (1, 1) NOT NULL,
    [UserID]         INT            NOT NULL,
    [Value]          NVARCHAR (100) NOT NULL,
    [CreationDate]   DATETIME2 (2)  NOT NULL,
    [ExpirationDate] DATETIME2 (2)  NULL,
    CONSTRAINT [PK_RefreshToken] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_RefreshToken_UserID] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([ID])
);

