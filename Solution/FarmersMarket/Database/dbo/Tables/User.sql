﻿CREATE TABLE [dbo].[User] (
    [ID]           INT            IDENTITY (1, 1) NOT NULL,
    [LastName]     NVARCHAR (100) NOT NULL,
    [FirstName]    NVARCHAR (100) NOT NULL,
    [Address]      NVARCHAR (100) NULL,
    [City]         NVARCHAR (100) NULL,
    [RoleID]       INT            NOT NULL,
    [Email]        VARCHAR (80)   NOT NULL,
    [PasswordHash] VARCHAR (100)  NOT NULL,
    [Username]     VARCHAR (100)  NOT NULL,
    [IsDeleted]    BIT            CONSTRAINT [DF_User_IsDeleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_User_RoleId] FOREIGN KEY ([RoleID]) REFERENCES [dbo].[Role] ([ID]),
    UNIQUE NONCLUSTERED ([Username] ASC)
);





