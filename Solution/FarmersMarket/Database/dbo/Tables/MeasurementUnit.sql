﻿CREATE TABLE [dbo].[MeasurementUnit] (
    [ID]    INT            IDENTITY (1, 1) NOT NULL,
    [Title] NVARCHAR (100) NULL,
    [Label] VARCHAR (10)   NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);

