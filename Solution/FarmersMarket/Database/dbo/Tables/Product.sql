﻿CREATE TABLE [dbo].[Product] (
    [ID]                INT            IDENTITY (1, 1) NOT NULL,
    [Title]             NVARCHAR (100) NOT NULL,
    [Quantity]          DECIMAL (5, 2) NOT NULL,
    [Price]             DECIMAL (5, 2) NOT NULL,
    [Description]       NVARCHAR (500) NOT NULL,
    [MeasurementUnitID] INT            CONSTRAINT [DF_Product_MeasurementUnitID] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED ([ID] ASC),
    FOREIGN KEY ([MeasurementUnitID]) REFERENCES [dbo].[MeasurementUnit] ([ID])
);







