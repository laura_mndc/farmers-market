﻿CREATE TABLE [dbo].[InvoiceProduct] (
    [ID]        INT            IDENTITY (1, 1) NOT NULL,
    [ProductID] INT            NOT NULL,
    [InvoiceID] INT            NOT NULL,
    [Quantity]  DECIMAL (5, 2) NOT NULL,
    [Price]     DECIMAL (5, 2) NOT NULL,
    CONSTRAINT [PK_InvoiceProduct] PRIMARY KEY CLUSTERED ([InvoiceID] ASC, [ProductID] ASC),
    CONSTRAINT [FK_InvoiceProduct_InvoiceId] FOREIGN KEY ([InvoiceID]) REFERENCES [dbo].[Invoice] ([ID]),
    CONSTRAINT [FK_InvoiceProduct_ProductId] FOREIGN KEY ([ProductID]) REFERENCES [dbo].[Product] ([ID])
);



