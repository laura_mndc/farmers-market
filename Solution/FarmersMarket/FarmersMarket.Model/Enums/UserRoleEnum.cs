﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmersMarket.Model.Enums
{
    public enum UserRoleEnum
    {
        Admin = 1,
        Client = 2
    }
}
