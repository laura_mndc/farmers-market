﻿using System;
using System.Collections.Generic;


namespace FarmersMarket.Model.DTOs
{
    public class ProductGridInfoDto
    {
        public int TotalCount { get; set; }
        public IEnumerable<ProductDto> Products { get; set; }
    }
}
