﻿

namespace FarmersMarket.Model.DTOs
{
    public class UserInfoDto : BaseUserDto
    {
        public int RoleId { get; set; }
        public string Role { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
    }
}
