﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmersMarket.Model.DTOs
{
    public class UserAccountDto : BaseUserDto
    {
        public string JwtToken { get; set; }
        public string RefreshTokens { get; set; }
    }
}
