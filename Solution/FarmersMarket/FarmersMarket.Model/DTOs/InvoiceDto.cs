﻿using System;
using System.Collections.Generic;

namespace FarmersMarket.Model.DTOs
{
    public class InvoiceDto
    {
        public int Id { get; set; }
        public DateTime IssuedOn { get; set; }
        public DateTime? PaidOn { get; set; }
        public int BuyerId { get; set; }
        public string BuyerName { get; set; }
        public IEnumerable<InvoiceProductDto> InvoiceItems { get; set; }

        public decimal TotalPrice { get; set; }


    }
}
