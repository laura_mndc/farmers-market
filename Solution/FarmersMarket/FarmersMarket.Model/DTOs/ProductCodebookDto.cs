﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmersMarket.Model.DTOs
{
    public class ProductCodebookDto
    {
        public IEnumerable<DropdownDto> MeasurementUnits { get; set; }
    }
}
