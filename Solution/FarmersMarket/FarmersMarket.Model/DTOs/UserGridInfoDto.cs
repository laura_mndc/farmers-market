﻿using System.Collections.Generic;


namespace FarmersMarket.Model.DTOs
{
    public class UserGridInfoDto
    {
        public int TotalCount { get; set; }
        public IEnumerable<UserInfoDto> Users { get; set; }
    }
}
