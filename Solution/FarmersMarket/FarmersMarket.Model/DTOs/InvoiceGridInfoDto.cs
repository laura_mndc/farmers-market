﻿
using System.Collections.Generic;

namespace FarmersMarket.Model.DTOs
{
    public class InvoiceGridInfoDto
    {
        public int TotalCount { get; set; }
        public IEnumerable<InvoiceDto> Orders { get; set; }
    }
}
