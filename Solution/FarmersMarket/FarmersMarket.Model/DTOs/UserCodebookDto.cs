﻿using System.Collections.Generic;

namespace FarmersMarket.Model.DTOs
{
    public class UserCodebookDto
    {
        public IEnumerable<DropdownDto> Roles { get; set; }
    }
}
