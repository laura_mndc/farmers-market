﻿namespace FarmersMarket.Model.DTOs
{
    public class DropdownDto
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
