﻿

namespace FarmersMarket.Model.DTOs
{
    public class InvoiceProductDto
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string ProductTitle { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public string MeasurementUnit { get; set; }
        public decimal TotalProductPrice => Quantity * Price;
    }
}
