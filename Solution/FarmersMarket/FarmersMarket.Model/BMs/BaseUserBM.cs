﻿using System.ComponentModel.DataAnnotations;

namespace FarmersMarket.Model.BMs
{
    public class BaseUserBM
    {
        [Required]
        public int RoleId { get; set; }

        [Required]
        [EmailAddress]
        [MaxLength(80)]
        public string Email { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [Required]
        [MaxLength(80)]
        public string Surname { get; set; }

        [Required]
        [MaxLength(30)]
        public string Username { get; set; }

        public string Address { get; set; }

        public string City { get; set; }
    }
}
