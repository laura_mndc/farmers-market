﻿using System.ComponentModel.DataAnnotations;

namespace FarmersMarket.Model.BMs
{
    public class EditUserBM : BaseUserBM
    {
        [Required]
        public int Id { get; set; }
    }
}
