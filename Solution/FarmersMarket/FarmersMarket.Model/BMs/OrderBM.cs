﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmersMarket.Model.BMs
{
    public class OrderBM
    {
        public IEnumerable<OrderItemBM> Products { get; set; }
    }
}
