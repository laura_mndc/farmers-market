﻿
using System.ComponentModel.DataAnnotations;


namespace FarmersMarket.Model.BMs
{
    public class BaseProductBM
    {
        [Required]
        public int MeasurementUnitId { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public decimal Quantity { get; set; }
        [Required]
        public decimal Price { get; set; }
        public string Description { get; set; }
    }
}
