﻿using FarmersMarket.Model.BMs;
using FarmersMarket.Service.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace FarmersMarket.Web.Controllers
{
    [Route("api/product")]
    [ApiController]
    public class ProductController : CustomBaseController
    {
        private readonly IProductService productService;

        public ProductController(IProductService productService)
        {
            this.productService = productService;
        }

        [HttpGet("")]
        public async Task<IActionResult> GetProducts([FromQuery] string searchValue = null, string sortOrder = "asc", int page = 1, int pageSize = 10)
        {
            var result = await productService.GetProducts(sortOrder, page, pageSize, searchValue);
            return Ok(result);
        }

        [HttpGet("by-id")]
        public async Task<IActionResult> GetProductsbyId([FromQuery] int[] productIds)
        {
            var result = await productService.GetProductsByIds(productIds);
            return Ok(result);
        }

        [HttpGet("{id}")]
        public IActionResult GetProductDetails([FromRoute] int id)
        {
            var result = productService.GetProductDetails(id);
            return Ok(result);
        }

        [HttpPost("")]
        public IActionResult AddProduct(BaseProductBM product)
        {
            productService.InsertProduct(product);
            return Ok();
        }

        [HttpPatch("")]
        public IActionResult EditProduct(EditProductBM product)
        {
            productService.EditProduct(product);
            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteProduct([FromRoute] int id)
        {
            productService.DeleteProduct(id);
            return Ok();
        }

        [HttpGet("codebooks")]
        public IActionResult GetProductCodebooks()
        {
            var result = productService.GetProductCodebooks();
            return Ok(result);
        }

        [HttpPost("add-order")]
        public async Task<IActionResult> AddOrder(OrderBM order)
        {
            var result = await productService.InsertOrder(order, User.Id);
            return Ok(result);
        }
    }
}
