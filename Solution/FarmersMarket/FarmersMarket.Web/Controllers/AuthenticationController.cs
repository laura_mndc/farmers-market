﻿using FarmersMarket.Model.BMs;
using FarmersMarket.Model.DTOs;
using FarmersMarket.Service.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FarmersMarket.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : CustomBaseController
    {
        private readonly IAuthenticationService authenticationService;

        public AuthenticationController(IAuthenticationService authenticationService)
        {
            this.authenticationService = authenticationService;
        }

        [HttpPost("authenticate")]
        public ActionResult<UserAccountDto> Authenticate(AuthenticationBM model)
        {
            var response = authenticationService.Authenticate(model);
            return Ok(response);
        }

        [HttpPost("register")]
        public ActionResult<UserAccountDto> Register(RegistrationBM model)
        {
            authenticationService.Register(model);

            AuthenticationBM authenticationData = new()
            {
                Username = model.Username,
                Password = model.Password
            };
            var response = authenticationService.Authenticate(authenticationData);

            return Ok(response);
        }

        [HttpPost("refresh-token")]
        public ActionResult<UserAccountDto> RefreshToken()
        {
            Request.Headers.TryGetValue("RefreshToken", out StringValues refreshToken);

            var response = authenticationService.RefreshToken(refreshToken);
            return Ok(response);
        }
    }
}
