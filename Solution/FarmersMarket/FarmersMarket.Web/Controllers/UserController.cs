﻿using FarmersMarket.Model.BMs;
using FarmersMarket.Service.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace FarmersMarket.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : CustomBaseController
    {
        private readonly IUserService userService;

        public UserController(IUserService userService)
        {
            this.userService = userService;
        }

        [HttpGet("")]
        public async Task<IActionResult> GetUsers([FromQuery] string searchValue = null, string sortOrder = "asc", int page = 1, int pageSize = 10)
        {
            var result = await userService.GetUsers(sortOrder, page, pageSize, searchValue);
            return Ok(result);
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteUser([FromRoute] int id)
        {
            userService.DeleteUser(id);
            return Ok();
        }

        [HttpPost("edit")]
        public IActionResult EditUser(EditUserBM model)
        {
            userService.EditUser(model);
            return Ok();
        }

        [HttpGet("codebooks")]
        public IActionResult GetUserCodebooks()
        {
            var result = userService.GetUserCodebooks();
            return Ok(result);
        }

        [HttpGet("{id}")]
        public IActionResult GetUser([FromRoute] int id)
        {
            var result = userService.GetUser(id);
            return Ok(result);
        }


    }
}
