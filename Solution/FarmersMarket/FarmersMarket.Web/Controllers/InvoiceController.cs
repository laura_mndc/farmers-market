﻿
using Microsoft.AspNetCore.Mvc;
using FarmersMarket.Service.Services.Interfaces;
using System.Threading.Tasks;

namespace FarmersMarket.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InvoiceController : CustomBaseController
    {
        private readonly IInvoiceService invoiceService;

        public InvoiceController(IInvoiceService invoiceService)
        {
            this.invoiceService = invoiceService;
        }

        [HttpGet("")]
        public async Task<IActionResult> GetUsers([FromQuery] string searchValue = null, string sortOrder = "asc", int page = 1, int pageSize = 10)
        {
            var result = await invoiceService.GetOrders(sortOrder, page, pageSize, searchValue);
            return Ok(result);
        }


        [HttpGet("{id}")]
        public IActionResult GetInvoice([FromRoute] int id)
        {
            var result = invoiceService.GetInvoice(id);
            return Ok(result);
        }

        [HttpPatch("mark-as-paid")]
        public IActionResult MarkAsPaid([FromBody] int id)
        {
            invoiceService.SetInvoiceAsPaid(id);
            return Ok();
        }
    }
}
