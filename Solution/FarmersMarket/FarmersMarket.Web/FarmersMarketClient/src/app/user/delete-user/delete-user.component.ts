import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { first } from 'rxjs';
import { DeleteUserDialogData } from 'src/app/core/data-models/DeleteUserDialogData';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrls: ['./delete-user.component.scss'],
})
export class DeleteUserComponent implements OnInit {
  public id: number;
  public name: string;
  public surname: string;

  constructor(
    protected dialogRef: MatDialogRef<DeleteUserComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: DeleteUserDialogData,
    protected userService: UserService
  ) {
    this.id = this.data.id;
    this.name = this.data.name;
    this.surname = this.data.surname;
  }

  ngOnInit(): void {}

  public onNoClick() {
    this.dialogRef.close(false);
  }

  public onYesClick() {
    this.userService
      .deleteUser(this.id)
      .pipe(first())
      .subscribe(() => {
        {
          this.dialogRef.close(true);
        }
      });
  }
}
