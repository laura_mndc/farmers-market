import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute, Router } from '@angular/router';
import { debounceTime, fromEvent } from 'rxjs';
import { DeleteUserDialogData } from 'src/app/core/data-models/DeleteUserDialogData';
import { TokenService } from 'src/app/entry/services/token.service';

import { DeleteUserComponent } from '../delete-user/delete-user.component';
import { UserDataSourceService } from '../services/user-data-source.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
})
export class UserListComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('search') searchInput: ElementRef;

  public displayedColumns = ['name', 'surname', 'username', 'role', 'actions'];
  public dataSource: UserDataSourceService;
  public searchValue: string;
  protected oldSearchValue = '';

  constructor(
    protected userService: UserService,
    protected route: ActivatedRoute,
    protected router: Router,
    protected tokenService: TokenService,
    protected dialog: MatDialog
  ) {
    this.dataSource = new UserDataSourceService(this.userService);
  }

  ngAfterViewInit(): void {
    this.initSortingAndPagination();
    this.listenToSearchInput();
    this.getUsers();
  }

  protected initSortingAndPagination() {
    this.paginator.page.subscribe(() => this.getUsers());
    this.sort.sortChange.subscribe(() => this.resetPaginationAndGetUsers());
  }

  protected resetPaginationAndGetUsers() {
    this.paginator.pageIndex = 0;
    this.getUsers();
  }

  protected listenToSearchInput() {
    fromEvent(this.searchInput.nativeElement, 'keyup')
      .pipe(debounceTime(500))
      .subscribe(() => {
        this.searchValue = this.searchValue?.trim();
        if (this.searchValue != this.oldSearchValue) {
          this.resetPaginationAndGetUsers();
        }
        this.oldSearchValue = this.searchValue;
      });
  }

  protected getUsers() {
    this.dataSource.isLoading = true;
    this.dataSource.loadUsers(
      this.sort.direction,
      this.searchValue,
      this.paginator.pageIndex + 1,
      this.paginator.pageSize
    );
  }

  public openDeleteDialog(
    userId: number,
    userName: string,
    userSurname: string
  ) {
    var dialogData: DeleteUserDialogData = {
      id: userId,
      name: userName,
      surname: userSurname,
    };
    const dialogRef = this.dialog.open(DeleteUserComponent, {
      data: dialogData,
    });
    dialogRef.afterClosed().subscribe((isDeleted: boolean) => {
      if (isDeleted) {
        this.resetPaginationAndGetUsers();
      }
    });
  }
}
