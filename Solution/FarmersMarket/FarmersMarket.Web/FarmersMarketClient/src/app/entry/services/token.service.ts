import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { UserAccountDto } from '../../_autogenerated/userAccountDto';
import decode from 'jwt-decode';
import { TokenData } from 'src/app/core/data-models/TokenData';

@Injectable({ providedIn: 'root' })
export class TokenService {
  public userSubject: BehaviorSubject<UserAccountDto>;
  public user$: Observable<UserAccountDto>;

  constructor() {
    this.userSubject = new BehaviorSubject<UserAccountDto>(null!);
    this.user$ = this.userSubject.asObservable();
  }

  public get userValue(): UserAccountDto {
    return this.userSubject.value;
  }

  public createHttpHeaderWithRefreshToken() {
    var refreshToken = this.getRefreshTokens();

    const headerValue = {
      RefreshToken: refreshToken ?? '',
    };
    const requestOptions = {
      headers: new HttpHeaders(headerValue),
      withCredentials: true,
    };
    return requestOptions;
  }

  public saveTokenInfoToStorage(tokenInfo: UserAccountDto) {
    localStorage.setItem('jwtToken', tokenInfo.jwtToken);
    localStorage.setItem('refreshToken', tokenInfo.refreshTokens);
    localStorage.setItem('userId', tokenInfo.id.toString());
  }

  public getTimeoutForRefresh() {
    const jwtToken = JSON.parse(atob(this.userValue.jwtToken.split('.')[1]));
    // refresh the token a minute before it expires
    const expires = new Date(jwtToken.exp * 1000);
    const timeout = expires.getTime() - Date.now() - 60 * 1000;
    return timeout;
  }

  public getCurrentUserFromToken(): TokenData {
    const token = this.getJWTToken();
    const tokenPayload = decode<TokenData>(token as string);
    return tokenPayload;
  }

  public checkIfJwtTokenExists(): boolean {
    const token = this.getJWTToken();
    if (token) {
      return true;
    }
    return false;
  }

  public checkIfRefreshTokenExists(): boolean {
    const token = this.getRefreshTokens();
    if (token) {
      return true;
    }
    return false;
  }

  private getJWTToken() {
    return localStorage.getItem('jwtToken');
  }

  private getRefreshTokens() {
    return localStorage.getItem('refreshToken');
  }
}
