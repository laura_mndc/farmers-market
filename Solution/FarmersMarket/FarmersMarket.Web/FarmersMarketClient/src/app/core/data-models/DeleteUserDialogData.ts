export interface DeleteUserDialogData {
  id: number;
  name: string;
  surname: string;
}
