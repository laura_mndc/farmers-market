export interface DeleteProductDialogData {
  id: number;
  name: string;
}
