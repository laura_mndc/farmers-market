export class CartDataItem {
  public id: number;
  public title: string;
  public price: number;
  public amount: number;
  public total: number;
  public measurementUnit: string;
  constructor(
    id: number,
    title: string,
    price: number,
    amount: number,
    total: number,
    measurementUnit: string
  ) {
    this.id = id;
    this.title = title;
    this.price = price;
    this.amount = amount;
    this.total = total;
    this.measurementUnit = measurementUnit;
  }
}
