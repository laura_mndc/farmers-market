export interface CartData {
  id: number;
  amount: number;
}
