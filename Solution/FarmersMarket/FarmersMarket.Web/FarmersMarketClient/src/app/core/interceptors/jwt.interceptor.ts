import { Inject, Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  private apiCallsWithoutJwt = [
    'api/authentication/authenticate',
    'api/authentication/register',
  ];

  constructor(@Inject('API_BASE_URL') public baseUrl: string) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // add auth header with jwt

    if (!this.apiCallsWithoutJwt.find((url) => request.url.includes(url))) {
      const jwtToken = localStorage.getItem('jwtToken');
      const isLoggedIn = jwtToken;
      const isApiUrl = request.url.startsWith(this.baseUrl);

      if (isLoggedIn && isApiUrl) {
        request = request.clone({
          setHeaders: { Authorization: `${jwtToken}` },
        });
      }
    }
    return next.handle(request);
  }
}
