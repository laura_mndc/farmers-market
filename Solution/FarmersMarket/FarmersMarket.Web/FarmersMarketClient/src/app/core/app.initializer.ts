import { first, firstValueFrom, tap } from 'rxjs';
import { AccountService } from '../entry/services/account.service';

export function appInitializer(accountService: AccountService) {
  return () => {
    return firstValueFrom(
      accountService.refreshTokenIfExists().pipe(
        tap((tokenInfo) => {
          if (tokenInfo) {
            localStorage.setItem('jwtToken', tokenInfo.jwtToken);
            localStorage.setItem('refreshToken', tokenInfo.refreshTokens);
            localStorage.setItem('userId', tokenInfo.id.toString());
          }
        })
      )
    );
  };
}
