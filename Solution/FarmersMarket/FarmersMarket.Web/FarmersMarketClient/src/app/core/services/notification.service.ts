import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root',
})
export class NotificationService {
  private baseOptions: MatSnackBarConfig = {
    duration: 2000,
    horizontalPosition: 'end',
    verticalPosition: 'top',
  };

  private successOptions: MatSnackBarConfig = Object.assign(
    {
      panelClass: ['bg-success'],
    },
    this.baseOptions
  );
  private errorOptions: MatSnackBarConfig = Object.assign(
    {
      panelClass: ['bg-danger'],
    },
    this.baseOptions
  );
  private infoOptions: MatSnackBarConfig = Object.assign(
    {
      panelClass: ['bg-light'],
    },
    this.baseOptions
  );

  constructor(private snack: MatSnackBar) {}

  public success(message: string): void {
    if (!message) {
      message = 'Success';
    }

    this.snack.open(message, '', this.successOptions);
  }

  public error(message: string): void {
    if (!message) {
      message = 'Error';
    }
    this.snack.open(message, '', this.errorOptions);
  }

  public info(message: string): void {
    if (!message) {
      message = 'Info';
    }

    this.snack.open(message, '', this.infoOptions);
  }
}
