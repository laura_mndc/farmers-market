import { Component } from '@angular/core';
import { TokenService } from './entry/services/token.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'FarmersMarketClient';
  public isLoggedIn: boolean = false;

  constructor(private tokenService: TokenService) {
    this.tokenService.user$.subscribe(
      (user) => (this.isLoggedIn = user ? true : false)
    );
  }
}
