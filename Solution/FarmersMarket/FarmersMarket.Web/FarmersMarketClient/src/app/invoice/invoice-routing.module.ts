import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InvoiceListComponent } from './invoice-list/invoice-list.component';
import { InvoiceProductDetailsComponent } from './invoice-product-details/invoice-product-details.component';

const routes: Routes = [
  {
    path: '',

    children: [
      {
        path: 'all-invoices',
        component: InvoiceListComponent,
        pathMatch: 'full',
      },
      {
        path: 'details/:id',
        component: InvoiceProductDetailsComponent,
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InvoiceRoutingModule {}
