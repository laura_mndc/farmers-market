import {
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute, Router } from '@angular/router';
import { debounceTime, first, fromEvent } from 'rxjs';
import { InvoiceDataSourceService } from '../services/invoice-data-source.service';
import { InvoiceService } from '../services/invoice.service';

@Component({
  selector: 'app-invoice-list',
  templateUrl: './invoice-list.component.html',
  styleUrls: ['./invoice-list.component.scss'],
})
export class InvoiceListComponent implements OnDestroy {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('search') searchInput: ElementRef;

  public displayedColumns = [
    'buyerName',
    'issuedOn',
    'totalPrice',
    'isPaid',
    'actions',
  ];
  public dataSource: InvoiceDataSourceService;
  public searchValue: string;
  protected oldSearchValue = '';

  constructor(
    protected invoiceService: InvoiceService,
    protected route: ActivatedRoute,
    protected router: Router,
    protected dialog: MatDialog
  ) {
    this.dataSource = new InvoiceDataSourceService(this.invoiceService);
  }
  ngOnDestroy(): void {
    this.sort.sortChange.unsubscribe();
    this.paginator.page.unsubscribe();
  }

  ngAfterViewInit(): void {
    this.initSortingAndPagination();
    this.listenToSearchInput();
    this.getInvoices();
  }

  protected initSortingAndPagination() {
    this.paginator.page.subscribe(() => this.getInvoices());
    this.sort.sortChange.subscribe(() => this.resetPaginationAndGetInvoices());
  }

  protected resetPaginationAndGetInvoices() {
    this.paginator.pageIndex = 0;
    this.getInvoices();
  }

  protected listenToSearchInput() {
    fromEvent(this.searchInput.nativeElement, 'keyup')
      .pipe(debounceTime(500))
      .subscribe(() => {
        this.searchValue = this.searchValue?.trim();
        if (this.searchValue != this.oldSearchValue) {
          this.resetPaginationAndGetInvoices();
        }
        this.oldSearchValue = this.searchValue;
      });
  }

  protected getInvoices() {
    this.dataSource.isLoading = true;
    this.dataSource.loadInvoices(
      this.sort.direction,
      this.searchValue,
      this.paginator.pageIndex + 1,
      this.paginator.pageSize
    );
  }

  public markAsPaid(id: number) {
    this.invoiceService
      .markAsPaid(id)
      .pipe(first())
      .subscribe(() => {
        this.resetPaginationAndGetInvoices();
      });
  }
}
