﻿import {InvoiceProductDto} from './invoiceProductDto';

// ----------------------------------------------
//  Interface and model
// ----------------------------------------------
export interface IInvoiceDto  {
    id: number;
    issuedOn: Date;
    paidOn: Date;
    buyerId: number;
    buyerName: string;
    invoiceItems: InvoiceProductDto[];
    totalPrice: number;
}

export class InvoiceDto  implements IInvoiceDto {
    public id: number;
    public issuedOn: Date;
    public paidOn: Date;
    public buyerId: number;
    public buyerName: string;
    public invoiceItems: InvoiceProductDto[];
    public totalPrice: number;

    constructor(id: number, issuedOn: Date, paidOn: Date, buyerId: number, buyerName: string, invoiceItems: InvoiceProductDto[], totalPrice: number) {
        
        this.id = id;
        this.issuedOn = issuedOn;
        this.paidOn = paidOn;
        this.buyerId = buyerId;
        this.buyerName = buyerName;
        this.invoiceItems = invoiceItems;
        this.totalPrice = totalPrice;
    }
}

 
