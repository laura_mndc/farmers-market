﻿import {ProductDto} from './productDto';

// ----------------------------------------------
//  Interface and model
// ----------------------------------------------
export interface IProductGridInfoDto  {
    totalCount: number;
    products: ProductDto[];
}

export class ProductGridInfoDto  implements IProductGridInfoDto {
    public totalCount: number;
    public products: ProductDto[];

    constructor(totalCount: number, products: ProductDto[]) {
        
        this.totalCount = totalCount;
        this.products = products;
    }
}

 
