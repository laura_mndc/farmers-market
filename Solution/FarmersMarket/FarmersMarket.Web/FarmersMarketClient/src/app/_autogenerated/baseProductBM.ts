﻿
// ----------------------------------------------
//  Interface and model
// ----------------------------------------------
export interface IBaseProductBM  {
    measurementUnitId: number;
    title: string;
    quantity: number;
    price: number;
    description: string;
}

export class BaseProductBM  implements IBaseProductBM {
    public measurementUnitId: number;
    public title: string;
    public quantity: number;
    public price: number;
    public description: string;

    constructor(measurementUnitId: number, title: string, quantity: number, price: number, description: string) {
        
        this.measurementUnitId = measurementUnitId;
        this.title = title;
        this.quantity = quantity;
        this.price = price;
        this.description = description;
    }
}

 
