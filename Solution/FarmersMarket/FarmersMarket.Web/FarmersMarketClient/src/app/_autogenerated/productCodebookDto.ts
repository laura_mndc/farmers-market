﻿import {DropdownDto} from './dropdownDto';

// ----------------------------------------------
//  Interface and model
// ----------------------------------------------
export interface IProductCodebookDto  {
    measurementUnits: DropdownDto[];
}

export class ProductCodebookDto  implements IProductCodebookDto {
    public measurementUnits: DropdownDto[];

    constructor(measurementUnits: DropdownDto[]) {
        
        this.measurementUnits = measurementUnits;
    }
}

 
