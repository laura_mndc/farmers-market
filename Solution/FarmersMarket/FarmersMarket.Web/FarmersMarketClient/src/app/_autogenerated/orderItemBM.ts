﻿
// ----------------------------------------------
//  Interface and model
// ----------------------------------------------
export interface IOrderItemBM  {
    id: number;
    amount: number;
}

export class OrderItemBM  implements IOrderItemBM {
    public id: number;
    public amount: number;

    constructor(id: number, amount: number) {
        
        this.id = id;
        this.amount = amount;
    }
}

 
