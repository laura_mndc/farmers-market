﻿import {UserInfoDto} from './userInfoDto';

// ----------------------------------------------
//  Interface and model
// ----------------------------------------------
export interface IUserGridInfoDto  {
    totalCount: number;
    users: UserInfoDto[];
}

export class UserGridInfoDto  implements IUserGridInfoDto {
    public totalCount: number;
    public users: UserInfoDto[];

    constructor(totalCount: number, users: UserInfoDto[]) {
        
        this.totalCount = totalCount;
        this.users = users;
    }
}

 
