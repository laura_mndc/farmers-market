﻿import {IBaseProductBM, BaseProductBM} from './baseProductBM';

// ----------------------------------------------
//  Interface and model
// ----------------------------------------------
export interface IEditProductBM extends IBaseProductBM {
    id: number;
}

export class EditProductBM extends BaseProductBM implements IEditProductBM {
    public id: number;

    constructor(id: number, measurementUnitId: number, title: string, quantity: number, price: number, description: string) {
        super(measurementUnitId, title, quantity, price, description);
        this.id = id;
    }
}

 
