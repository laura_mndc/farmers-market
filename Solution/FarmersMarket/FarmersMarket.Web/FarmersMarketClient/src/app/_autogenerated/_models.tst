${
    using Typewriter.Extensions.Types;
    Template(Settings settings)
    {     
       settings.OutputFilenameFactory = (file) => {
                  var toFolder = "";

                  string fullPath = System.IO.Path.GetDirectoryName(file.FullName).TrimEnd(System.IO.Path.DirectorySeparatorChar);
                  string fromFolder = fullPath.Split(System.IO.Path.DirectorySeparatorChar).Last();

                  if (file.Enums.Count > 0){
                      toFolder = toFolder + EnumsFolderFS;
                  }
                  
                  var f = file.Name.Replace(".cs", ".ts");
                  f = $"{toFolder}{f[0].ToString().ToLower()}{f.Substring(1)}";
                  return f;
        };
       settings.IncludeCurrentProject().IncludeProject("FarmersMarket.Model"); 
    }

    static string EnumsFolderFS ="";
    static string EnumsFolderTS ="";
     
    static string DebugInfo = "";
    string PrintDebugInfo(File f){
      if (string.IsNullOrEmpty(DebugInfo)) {
         return "";
      }
      return "/*" + Environment.NewLine + "Template debug info: " + DebugInfo + Environment.NewLine + "*/";
    }

	  Type CalculatedType(Type t)
    {
        var type = t;
        while (!type.IsEnumerable && type.IsGeneric) {
            type = type.Unwrap();
        }
        return type;
    }

    Class GetBaseClassIfExists(Class c) {
      var hasBase = c.BaseClass!=null;
      var baseIsAbstractWithoutProps = hasBase && c.BaseClass.IsAbstract && c.BaseClass.Properties.Count()==0;
      var r = hasBase && !baseIsAbstractWithoutProps ? c.BaseClass:null;
      return r;
    }

    string GetBaseClassWithTypeIfExists(Class c) {
      var hasBase = c.BaseClass!=null;
      var baseIsAbstractWithoutProps = hasBase && c.BaseClass.IsAbstract && c.BaseClass.Properties.Count()==0;
      var r = hasBase && !baseIsAbstractWithoutProps ? c.BaseClass + c.BaseClass.TypeArguments :"";
      return r;
    }

    Type[] CalculatedBaseClassModelTypes(Class c)
    {
        if (c.BaseClass == null) {
            return null;
        }

        // get base class name if it exists
        string baseClassName = c.BaseClass != null ? c.BaseClass.Name : "";
        
        var allTypes = c.BaseClass.Properties
            .Select(m => CalculatedType(m.Type))            
            .Where(t => t != null && t.Name != baseClassName && (t.IsDefined || (t.IsEnumerable && !t.IsPrimitive))) // avoid importing base class (it will be imported with calculated base)
            .ToLookup(t => t.ClassName(), t => t);

        var retVal = allTypes                    
                    .ToDictionary(l => l.Key, l => l.First())
                    .Select(kvp => kvp.Value)
                    .Where(kvp =>
                        kvp.Name != "T[]" && kvp.Name != c.Name && kvp.Name != c.Name + "[]" && kvp.Name != "T" 
                     ) // prevention of importing generic
                    .ToArray();

        return retVal;
    } 

 
    Type[] CalculatedModelTypes(Class c)
    {
        
        string baseClassName = c.BaseClass != null ? c.BaseClass.Name : "";
        var allTypes = c.Properties
            .Select(m => CalculatedType(m.Type))            
            .Where(t => t != null && t.Name != baseClassName && (t.IsDefined || (t.IsEnumerable && !t.IsPrimitive))) // avoid importing base class (it will be imported with calculated base)
            .ToLookup(t => t.ClassName(), t => t);

        var retVal = allTypes                    

                    .ToDictionary(l => l.Key, l => l.First())
                    .Select(kvp => kvp.Value)
                    .Where(kvp =>
                        kvp.Name != "T[]" && kvp.Name != c.Name && kvp.Name != c.Name + "[]" && kvp.Name != "T" 
                     ) 
                    .ToArray();

        return retVal;
    }

    string CalculatedTypeName(Type t)
    {
        var type = CalculatedType(t);
        return type != null ? type.Name : "void";
    }

    string TypeMap(Property property)
    {
        var type = property.Type;

        if (type.IsPrimitive)
        {
            return type.IsDate ?
                $"new Date(data.{UpperCasePropertyName(property)})" :
                $"data.{UpperCasePropertyName(property)}";
        }
        else
        {
            return type.IsEnumerable ?
                $"data.{UpperCasePropertyName(property)}.map(i => new {type.Name.TrimEnd('[', ']')}(i))" :
                $"new {type.Name}(data.{UpperCasePropertyName(property)})";
        }
    }

    string UpperCasePropertyName(Property property)
    {
       return property.Name[0].ToString().ToUpperInvariant() + property.Name.Substring(1);
    }
    
    string LowerCasePropertyName(Property property)
    {
       return property.Name[0].ToString().ToLowerInvariant() + property.Name.Substring(1);
    }

    string LowerCasePropertyNameWithAttribute(Property property)
    {
      var stringify = property.Attributes.Any(a => a.Name == "TypewriterJsonStringify");
      var prop = property.Name[0].ToString().ToLowerInvariant() + property.Name.Substring(1);

       return stringify? $"JSON.stringify({prop})" : prop;
    }

    string GetPropertiesSeparatorIfNeeded(Class c)
    {
      if (GetPropertiesAsArguments(c) != "" && GetBaseClassPropertiesAsArguments(c) != "")
      {
        return ", ";
      } else {
        return "";
      }
    }

    string GetPropertiesAsArguments(Class c)
    {
        var result = "";
        for(int i = 0; i< c.Properties.Count; i++)
        {
            var separator = (i == c.Properties.Count -1) ? "" : ", ";
            var prop = c.Properties[i];
            result += $"{LowerCasePropertyName(prop)}: {prop.Type}{separator}";
        }
        return result;
    }

    string GetBaseClassPropertiesAsArguments(Class c)
    {
      var hasBase = c.BaseClass!=null;
      if (!hasBase) {
        return "";
      }
      var baseClass = c.BaseClass;
      var result = "";
      for(int i = 0; i< baseClass.Properties.Count; i++)
      {
          var separator = (i == baseClass.Properties.Count -1) ? "" : ", ";
          var prop = baseClass.Properties[i];
          result += $"{LowerCasePropertyName(prop)}: {prop.Type}{separator}";
      }
      return result;
    }

    string GetBaseClassPropertiesForConstrucor(Class c)
    {
      var hasBase = c.BaseClass!=null;
      if (!hasBase) {
        return "";
      }
      var baseClass = c.BaseClass;
      var result = "";
      for(int i = 0; i< baseClass.Properties.Count; i++)
      {
          var separator = (i == baseClass.Properties.Count -1) ? "" : ", ";
          var prop = baseClass.Properties[i];
          result += $"{LowerCasePropertyName(prop)}{separator}";
      }
      return "super(" + result + ");";
    }

    string LowerCaseTypeName (Type t){
       var name = t.Name[0].ToString().ToLower() + t.Name.Substring(1);
       name = (t.Unwrap().IsEnum ? EnumsFolderTS : "") + name;
       return CalculateName(name);
    }

   string LowerCaseClassName (Class t){
       var retVal = t.Name[0].ToString().ToLower() + t.Name.Substring(1);
       return retVal;
    }

    string CalculateName(string name){

        string result = name;
        int index = result.IndexOf('<');
        result = index == -1 ? result : result.Substring(0, index);
        index = result.IndexOf('[');
        result = index == -1 ? result : result.Substring(0, index);
        return result;
    }
    
    string GetEnumValue(EnumValue enumItem){
      var desc = enumItem.Attributes.Where(a => a.Name == "EnumAsStringValue").FirstOrDefault();
      return desc != null ? $"'{desc.Value}'" : $"{enumItem.Value}";
    }
    
  
}$Classes(f=> (f.Namespace.StartsWith("FarmersMarket.Model")))[$CalculatedModelTypes[import {$ClassName} from './$LowerCaseTypeName';
]$CalculatedBaseClassModelTypes[import {$ClassName} from './$LowerCaseTypeName';
]$GetBaseClassIfExists[import {I$Name, $Name} from './$LowerCaseClassName';
]
// ----------------------------------------------
//  Interface and model
// ----------------------------------------------
export interface I$Name$TypeParameters $GetBaseClassIfExists[extends I]$GetBaseClassWithTypeIfExists {$Properties[
    $LowerCasePropertyName: $Type;]
}

export class $Name$TypeParameters $GetBaseClassIfExists[extends ]$GetBaseClassWithTypeIfExists implements I$Name$TypeParameters {$Properties[
    public $LowerCasePropertyName: $Type;]

    constructor($GetPropertiesAsArguments$GetPropertiesSeparatorIfNeeded$GetBaseClassPropertiesAsArguments) {
        $GetBaseClassPropertiesForConstrucor$Properties[
        this.$LowerCasePropertyName = $LowerCasePropertyNameWithAttribute;]
    }
}]
$Enums(f=>(f.Namespace.StartsWith("FarmersMarket.Model") && !f.Attributes.Select(a=> a.Name.ToLower()).Contains("typewriterignore")))[
// ----------------------------------------------
//  Enum: $Name
// ----------------------------------------------
export enum $Name {
          $Values[$Name = $GetEnumValue][,
          ]
}]
$PrintDebugInfo 
