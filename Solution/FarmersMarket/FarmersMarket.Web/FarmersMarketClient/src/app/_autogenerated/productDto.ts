﻿
// ----------------------------------------------
//  Interface and model
// ----------------------------------------------
export interface IProductDto  {
    id: number;
    title: string;
    quantity: number;
    price: number;
    description: string;
    measurementUnitId: number;
    measurementUnit: string;
}

export class ProductDto  implements IProductDto {
    public id: number;
    public title: string;
    public quantity: number;
    public price: number;
    public description: string;
    public measurementUnitId: number;
    public measurementUnit: string;

    constructor(id: number, title: string, quantity: number, price: number, description: string, measurementUnitId: number, measurementUnit: string) {
        
        this.id = id;
        this.title = title;
        this.quantity = quantity;
        this.price = price;
        this.description = description;
        this.measurementUnitId = measurementUnitId;
        this.measurementUnit = measurementUnit;
    }
}

 
