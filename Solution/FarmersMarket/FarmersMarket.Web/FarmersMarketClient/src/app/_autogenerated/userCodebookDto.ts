﻿import {DropdownDto} from './dropdownDto';

// ----------------------------------------------
//  Interface and model
// ----------------------------------------------
export interface IUserCodebookDto  {
    roles: DropdownDto[];
}

export class UserCodebookDto  implements IUserCodebookDto {
    public roles: DropdownDto[];

    constructor(roles: DropdownDto[]) {
        
        this.roles = roles;
    }
}

 
