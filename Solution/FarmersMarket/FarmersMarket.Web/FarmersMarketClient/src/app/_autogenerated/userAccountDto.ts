﻿import {IBaseUserDto, BaseUserDto} from './baseUserDto';

// ----------------------------------------------
//  Interface and model
// ----------------------------------------------
export interface IUserAccountDto extends IBaseUserDto {
    jwtToken: string;
    refreshTokens: string;
}

export class UserAccountDto extends BaseUserDto implements IUserAccountDto {
    public jwtToken: string;
    public refreshTokens: string;

    constructor(jwtToken: string, refreshTokens: string, id: number, name: string, surname: string, username: string) {
        super(id, name, surname, username);
        this.jwtToken = jwtToken;
        this.refreshTokens = refreshTokens;
    }
}

 
