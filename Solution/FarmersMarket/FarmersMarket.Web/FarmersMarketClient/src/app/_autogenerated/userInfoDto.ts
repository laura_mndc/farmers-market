﻿import {IBaseUserDto, BaseUserDto} from './baseUserDto';

// ----------------------------------------------
//  Interface and model
// ----------------------------------------------
export interface IUserInfoDto extends IBaseUserDto {
    roleId: number;
    role: string;
    email: string;
    address: string;
    city: string;
}

export class UserInfoDto extends BaseUserDto implements IUserInfoDto {
    public roleId: number;
    public role: string;
    public email: string;
    public address: string;
    public city: string;

    constructor(roleId: number, role: string, email: string, address: string, city: string, id: number, name: string, surname: string, username: string) {
        super(id, name, surname, username);
        this.roleId = roleId;
        this.role = role;
        this.email = email;
        this.address = address;
        this.city = city;
    }
}

 
