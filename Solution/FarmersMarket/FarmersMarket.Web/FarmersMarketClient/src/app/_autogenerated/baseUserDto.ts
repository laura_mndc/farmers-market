﻿
// ----------------------------------------------
//  Interface and model
// ----------------------------------------------
export interface IBaseUserDto  {
    id: number;
    name: string;
    surname: string;
    username: string;
}

export class BaseUserDto  implements IBaseUserDto {
    public id: number;
    public name: string;
    public surname: string;
    public username: string;

    constructor(id: number, name: string, surname: string, username: string) {
        
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.username = username;
    }
}

 
