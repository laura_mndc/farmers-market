﻿
// ----------------------------------------------
//  Interface and model
// ----------------------------------------------
export interface IBaseUserBM  {
    roleId: number;
    email: string;
    name: string;
    surname: string;
    username: string;
    address: string;
    city: string;
}

export class BaseUserBM  implements IBaseUserBM {
    public roleId: number;
    public email: string;
    public name: string;
    public surname: string;
    public username: string;
    public address: string;
    public city: string;

    constructor(roleId: number, email: string, name: string, surname: string, username: string, address: string, city: string) {
        
        this.roleId = roleId;
        this.email = email;
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.address = address;
        this.city = city;
    }
}

 
