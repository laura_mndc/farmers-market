﻿
// ----------------------------------------------
//  Interface and model
// ----------------------------------------------
export interface IRegistrationBM  {
    name: string;
    surname: string;
    address: string;
    city: string;
    email: string;
    username: string;
    password: string;
    roleId: number;
}

export class RegistrationBM  implements IRegistrationBM {
    public name: string;
    public surname: string;
    public address: string;
    public city: string;
    public email: string;
    public username: string;
    public password: string;
    public roleId: number;

    constructor(name: string, surname: string, address: string, city: string, email: string, username: string, password: string, roleId: number) {
        
        this.name = name;
        this.surname = surname;
        this.address = address;
        this.city = city;
        this.email = email;
        this.username = username;
        this.password = password;
        this.roleId = roleId;
    }
}

 
