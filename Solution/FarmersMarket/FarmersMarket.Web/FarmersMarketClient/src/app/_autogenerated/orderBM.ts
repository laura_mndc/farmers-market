﻿import {OrderItemBM} from './orderItemBM';

// ----------------------------------------------
//  Interface and model
// ----------------------------------------------
export interface IOrderBM  {
    products: OrderItemBM[];
}

export class OrderBM  implements IOrderBM {
    public products: OrderItemBM[];

    constructor(products: OrderItemBM[]) {
        
        this.products = products;
    }
}

 
