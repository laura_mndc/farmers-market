﻿import {IBaseUserBM, BaseUserBM} from './baseUserBM';

// ----------------------------------------------
//  Interface and model
// ----------------------------------------------
export interface IEditUserBM extends IBaseUserBM {
    id: number;
}

export class EditUserBM extends BaseUserBM implements IEditUserBM {
    public id: number;

    constructor(id: number, roleId: number, email: string, name: string, surname: string, username: string, address: string, city: string) {
        super(roleId, email, name, surname, username, address, city);
        this.id = id;
    }
}

 
