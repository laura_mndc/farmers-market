﻿
// ----------------------------------------------
//  Interface and model
// ----------------------------------------------
export interface IAuthenticationBM  {
    username: string;
    password: string;
}

export class AuthenticationBM  implements IAuthenticationBM {
    public username: string;
    public password: string;

    constructor(username: string, password: string) {
        
        this.username = username;
        this.password = password;
    }
}

 
