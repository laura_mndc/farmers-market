﻿
// ----------------------------------------------
//  Interface and model
// ----------------------------------------------
export interface IInvoiceProductDto  {
    id: number;
    productId: number;
    productTitle: string;
    quantity: number;
    price: number;
    measurementUnit: string;
    totalProductPrice: number;
}

export class InvoiceProductDto  implements IInvoiceProductDto {
    public id: number;
    public productId: number;
    public productTitle: string;
    public quantity: number;
    public price: number;
    public measurementUnit: string;
    public totalProductPrice: number;

    constructor(id: number, productId: number, productTitle: string, quantity: number, price: number, measurementUnit: string, totalProductPrice: number) {
        
        this.id = id;
        this.productId = productId;
        this.productTitle = productTitle;
        this.quantity = quantity;
        this.price = price;
        this.measurementUnit = measurementUnit;
        this.totalProductPrice = totalProductPrice;
    }
}

 
