﻿import {InvoiceDto} from './invoiceDto';

// ----------------------------------------------
//  Interface and model
// ----------------------------------------------
export interface IInvoiceGridInfoDto  {
    totalCount: number;
    orders: InvoiceDto[];
}

export class InvoiceGridInfoDto  implements IInvoiceGridInfoDto {
    public totalCount: number;
    public orders: InvoiceDto[];

    constructor(totalCount: number, orders: InvoiceDto[]) {
        
        this.totalCount = totalCount;
        this.orders = orders;
    }
}

 
