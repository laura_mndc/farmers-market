import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { first } from 'rxjs';
import { DeleteProductDialogData } from 'src/app/core/data-models/DeleteProductDialogData';
import { NotificationService } from 'src/app/core/services/notification.service';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-delete-product',
  templateUrl: './delete-product.component.html',
  styleUrls: ['./delete-product.component.scss'],
})
export class DeleteProductComponent implements OnInit {
  public id: number;
  public name: string;

  constructor(
    protected dialogRef: MatDialogRef<DeleteProductComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: DeleteProductDialogData,
    protected productService: ProductService,
    protected notificationService: NotificationService
  ) {
    this.id = this.data.id;
    this.name = this.data.name;
  }

  ngOnInit(): void {}

  public onNoClick() {
    this.dialogRef.close(false);
  }

  public onYesClick() {
    this.productService
      .deleteProduct(this.id)
      .pipe(first())
      .subscribe(() => {
        {
          this.notificationService.success(
            `Product has been successfully deleted.`
          );
          this.dialogRef.close(true);
        }
      });
  }
}
