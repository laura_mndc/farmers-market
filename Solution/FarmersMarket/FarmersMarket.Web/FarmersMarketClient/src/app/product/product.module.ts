import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material/material.module';
import { ProductRoutingModule } from './product-routing.module';
import { ProductListComponent } from './product-list/product-list.component';
import { UpsertProductComponent } from './upsert-product/upsert-product.component';
import { DeleteProductComponent } from './delete-product/delete-product.component';
import { AddToCartComponent } from './add-to-cart/add-to-cart.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';

@NgModule({
  declarations: [
    ProductListComponent,
    UpsertProductComponent,
    DeleteProductComponent,
    AddToCartComponent,
    ShoppingCartComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    ProductRoutingModule,
    FormsModule,
  ],
  providers: [],
})
export class ProductModule {}
