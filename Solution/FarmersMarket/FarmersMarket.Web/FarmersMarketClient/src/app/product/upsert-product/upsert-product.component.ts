import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first, forkJoin } from 'rxjs';
import { NotificationService } from 'src/app/core/services/notification.service';
import { BaseProductBM } from 'src/app/_autogenerated/baseProductBM';
import { DropdownDto } from 'src/app/_autogenerated/dropdownDto';
import { EditProductBM } from 'src/app/_autogenerated/editProductBM';
import { ProductDto } from 'src/app/_autogenerated/productDto';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-upsert-product',
  templateUrl: './upsert-product.component.html',
  styleUrls: ['./upsert-product.component.scss'],
})
export class UpsertProductComponent {
  public isEdit: boolean = false;
  public productDetails: ProductDto;
  public unit: number;
  public productUnits: Array<DropdownDto>;
  constructor(
    public productService: ProductService,
    protected route: ActivatedRoute,
    protected router: Router,
    private fb: FormBuilder,
    protected notificationService: NotificationService
  ) {}

  formGroup: FormGroup;

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    if (id && !isNaN(Number(id))) {
      forkJoin([
        this.productService.getProductDetails(Number(id)),
        this.productService.getProductUnitCodebook(),
      ])
        .pipe(first())
        .subscribe(([details, unitCodebooks]) => {
          this.productDetails = details;
          this.productUnits = unitCodebooks.measurementUnits;
          this.unit = Number(this.productDetails.measurementUnitId);
          this.initForm();
        });
      this.isEdit = true;
    } else {
      this.productService
        .getProductUnitCodebook()
        .pipe(first())
        .subscribe((unitCodebooks) => {
          this.productUnits = unitCodebooks.measurementUnits;
          this.initForm();
        });
    }
  }

  initForm() {
    this.formGroup = this.fb.group({
      id: [this.productDetails?.id],
      title: [this.productDetails?.title || '', [Validators.required]],
      price: [this.productDetails?.price || null, [Validators.required]],
      quantity: [this.productDetails?.quantity || null],
      description: [this.productDetails?.description || ''],
      measurementUnitId: [this?.unit || '', [Validators.required]],
    });
  }

  onSubmit() {
    if (this.formGroup.invalid) {
      this.formGroup.markAllAsTouched();
      return;
    }
    let model;

    if (!this.isEdit) {
      model = this.formGroup.value as BaseProductBM;
      this.productService
        .addProduct(model)
        .pipe(first())
        .subscribe(() => {
          const returnUrl = 'products/all-products';
          this.router.navigate([returnUrl]);
          this.notificationService.success(
            `Product has been successfully added.`
          );
        });
    } else {
      model = this.formGroup.value as EditProductBM;
      this.productService
        .editProduct(model)
        .pipe(first())
        .subscribe(() => {
          const returnUrl = 'products/all-products';
          this.router.navigate([returnUrl]);
          this.notificationService.success(
            `Product has been successfully edited.`
          );
        });
    }
  }
}
