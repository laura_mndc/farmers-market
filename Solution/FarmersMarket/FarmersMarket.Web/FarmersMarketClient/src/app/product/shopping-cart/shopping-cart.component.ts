import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { first, Observable, tap } from 'rxjs';
import { CartData } from 'src/app/core/data-models/CartData';
import { CartDataItem } from 'src/app/core/data-models/CartDataItem';
import { NotificationService } from 'src/app/core/services/notification.service';
import { CartService } from '../services/cart.service';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.scss'],
})
export class ShoppingCartComponent implements OnInit, OnDestroy {
  public storageCart: CartData[];
  public cartItems: CartDataItem[];
  public cart$: Observable<CartDataItem[]>;
  public displayedColumns = [
    'title',
    'price',
    'amount',
    'totalperproduct',
    'actions',
  ];
  public totalInvoice: number = 0;

  constructor(
    protected productService: ProductService,
    protected cartService: CartService,
    protected router: Router,
    protected notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.storageCart = this.cartService.getCartData();
    this.cart$ = this.productService.getCartItems(this.storageCart).pipe(
      tap((products) => {
        this.cartItems = products;
        this.sumAllPrices();
      })
    );
  }

  ngOnDestroy(): void {
    this.cartService.updateCartAmounts(this.cartItems);
  }

  updateTotal(id: number) {
    let cartItem = this.cartItems.find((p) => p.id == id);
    if (cartItem) {
      cartItem.total = cartItem.amount * cartItem.price;
      this.sumAllPrices();
    }
  }

  removeFromCart(product: CartDataItem) {
    this.cartItems = this.cartItems.filter((item) => item.id != product.id);
    this.sumAllPrices();
    this.notificationService.success(
      `Product ${product.title} is removed from cart.`
    );
  }

  completeOrder() {
    this.productService
      .addOrder(this.cartItems)
      .pipe(first())
      .subscribe((isAdded: boolean) => {
        if (isAdded) {
          const returnUrl = 'products/all-products';
          this.router.navigate([returnUrl]);
          this.emptyCart();
          this.notificationService.success(`Order is successfully completed.`);
        }
      });
  }

  sumAllPrices() {
    this.totalInvoice = this.cartItems.reduce((accumulator, object) => {
      return accumulator + object.total;
    }, 0);
  }

  emptyCart() {
    this.cartItems = [];
    this.cartService.emptyCart();
  }
}
