﻿using FarmersMarket.DAL.Autogenerated.Model;


namespace FarmersMarket.Service.Helpers
{
    public static class UserValidator
    {
        public static void CheckIfUserExists(User user)
        {
            if (user == null || user.IsDeleted)
            {
                throw new CustomAppException("User not found.");
            }
        }

        public static void CheckIfUserRegistered(User user)
        {
            if (string.IsNullOrEmpty(user.PasswordHash))
            {
                throw new CustomAppException("User not registered.");
            }
        }

        public static void CheckIfPasswordsMatch(string password, string storedPasswordHash)
        {
            if (!BCrypt.Net.BCrypt.Verify(password, storedPasswordHash))
            {
                throw new CustomAppException("Wrong password.");
            }
        }

    }
}
