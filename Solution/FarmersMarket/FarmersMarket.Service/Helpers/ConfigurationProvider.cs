﻿using Microsoft.Extensions.Options;


namespace FarmersMarket.Service.Helpers
{
    public class ConfigurationProvider
    {
        public AppSettings AppSettings { get; set; }

        public ConfigurationProvider(IOptions<AppSettings> appSettings)
        {
            this.AppSettings = appSettings.Value;
        }

    }

}
