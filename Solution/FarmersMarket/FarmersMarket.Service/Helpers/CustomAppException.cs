﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmersMarket.Service.Helpers
{
    public class CustomAppException : Exception
    {
        public CustomAppException() : base() { }
        public CustomAppException(string message) : base(message) { }
    }
}
