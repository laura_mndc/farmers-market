﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmersMarket.Service.Helpers
{
    public class AppSettings
    {
        public string Secret { get; set; }
        public int JwtTokenMinutesDuration { get; set; }
        public int RefreshTokenDaysDuration { get; set; }
    }
}
