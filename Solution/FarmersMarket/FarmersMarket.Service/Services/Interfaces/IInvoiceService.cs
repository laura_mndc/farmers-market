﻿using FarmersMarket.Model.DTOs;
using System.Threading.Tasks;

namespace FarmersMarket.Service.Services.Interfaces
{
    public interface IInvoiceService
    {
        Task<InvoiceGridInfoDto> GetOrders(
          string sortOrder = "asc",
          int pageNumber = 1,
          int pageSize = 10,
          string searchValue = null
          );

        InvoiceDto GetInvoice(int id);
        void SetInvoiceAsPaid(int id);
    }
}
