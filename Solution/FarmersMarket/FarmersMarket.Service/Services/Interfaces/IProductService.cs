﻿using FarmersMarket.Model.BMs;
using FarmersMarket.Model.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FarmersMarket.Service.Services.Interfaces
{
    public interface IProductService
    {
        Task<ProductGridInfoDto> GetProducts(
          string sortOrder = "asc",
          int pageNumber = 1,
          int pageSize = 10,
          string searchValue = null
          );
        void InsertProduct(BaseProductBM product);
        void EditProduct(EditProductBM product);
        void DeleteProduct(int id);
        ProductDto GetProductDetails(int id);
        Task<IEnumerable<ProductDto>> GetProductsByIds(int[] productIds);
        Task<bool> InsertOrder(OrderBM order, int userId);
        ProductCodebookDto GetProductCodebooks();
    }
}
