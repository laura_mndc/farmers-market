﻿using FarmersMarket.Model.BMs;
using FarmersMarket.Model.DTOs;
using System.Threading.Tasks;

namespace FarmersMarket.Service.Services.Interfaces
{
    public interface IUserService
    {
        Task<UserGridInfoDto> GetUsers(
         string sortOrder = "asc",
         int pageNumber = 1,
         int pageSize = 10,
         string searchValue = null
         );
        void EditUser(EditUserBM model);
        UserInfoDto GetUser(int id);
        UserCodebookDto GetUserCodebooks();
        void DeleteUser(int id);
    }
}
