﻿using FarmersMarket.Model.BMs;
using FarmersMarket.Model.DTOs;

namespace FarmersMarket.Service.Services.Interfaces
{
    public interface IAuthenticationService
    {
        UserAccountDto Authenticate(AuthenticationBM model);
        UserAccountDto RefreshToken(string token);
        void Register(RegistrationBM model);
    }
}
