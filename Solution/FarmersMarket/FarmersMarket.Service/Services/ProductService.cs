﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using FarmersMarket.DAL.Autogenerated;
using FarmersMarket.DAL.Autogenerated.Model;
using FarmersMarket.Service.Services.Interfaces;
using FarmersMarket.Model.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using FarmersMarket.Model.BMs;
using FarmersMarket.Service.Helpers;
using System.Transactions;

namespace FarmersMarket.Service.Services
{
    public class ProductService : IProductService
    {
        protected List<int> pageSizes = new List<int>() { 10, 20 };

        protected readonly FarmersMarketDatabaseContext context;
        protected readonly IMapper mapper;


        public ProductService(
            FarmersMarketDatabaseContext context,
            IMapper mapper
            )
        {
            this.context = context;
            this.mapper = mapper;
        }
        public virtual async Task<ProductGridInfoDto> GetProducts(
          string sortOrder = "asc",
          int pageNumber = 1,
          int pageSize = 10,
          string searchValue = null
          )
        {
            pageSize = !pageSizes.Contains(pageSize) ? 10 : pageSize;
            pageNumber = pageNumber < 1 ? 1 : pageNumber;
            searchValue = searchValue?.Trim().ToLower();

            IQueryable<Product> query = context.Product;
            if (!string.IsNullOrEmpty(searchValue))
            {
                query = query.Where(b => b.Title.ToLower().Contains(searchValue));
            }

            var totalCount = query.Count();

            if (sortOrder == "asc")
            {
                query = query.OrderBy(b => b.Title);
            }
            else
            {
                query = query.OrderByDescending(b => b.Title);
            }

            query = query.Skip((pageNumber - 1) * pageSize)
                         .Take(pageSize);

            var gridInfo = new ProductGridInfoDto()
            {
                TotalCount = totalCount,
                Products = await query.ProjectTo<ProductDto>(mapper.ConfigurationProvider).ToListAsync()
            };

            return gridInfo;
        }

        public void InsertProduct(BaseProductBM product)
        {
            using (var scope = new TransactionScope())
            {
                try
                {
                    var dataToInsert = mapper.Map<Product>(product);
                    var result = context.Product.Add(dataToInsert);

                    context.SaveChanges();
                    scope.Complete();

                }
                catch (Exception ex)
                {
                    throw new CustomAppException(ex.Message);
                }
            }

        }

        public void EditProduct(EditProductBM product)
        {
            using (var scope = new TransactionScope())
            {
                try
                {

                    var result = context.Product.FirstOrDefault(p => p.Id == product.Id);
                    mapper.Map(product, result);
                    context.SaveChanges();
                    scope.Complete();

                }
                catch (Exception ex)
                {
                    throw new CustomAppException(ex.Message);
                }
            }

        }

        public void DeleteProduct(int id)
        {
            using (var scope = new TransactionScope())
            {
                try
                {
                    var result = context.Product.SingleOrDefault(p => p.Id == id);

                    if (result != null)
                    {
                        context.Product.Remove(result);
                        context.SaveChanges();
                    }
                    scope.Complete();

                }
                catch (Exception ex)
                {
                    throw new CustomAppException(ex.Message);
                }
            }

        }

        public ProductDto GetProductDetails(int id)
        {
            var product = context.Product.SingleOrDefault(p => p.Id == id);
            if (product == null)
            {
                throw new CustomAppException("Invalid product ID.");
            }
            return mapper.Map<ProductDto>(product);
        }

        public ProductCodebookDto GetProductCodebooks()
        {
            var units = context.MeasurementUnit.ToList();
            var dropdownunits = mapper.Map<List<DropdownDto>>(units);
            return new ProductCodebookDto { MeasurementUnits = dropdownunits };
        }

        public async Task<IEnumerable<ProductDto>> GetProductsByIds(int[] productIds)
        {
            if (productIds?.Length > 0)
            {
                return await context.Product.Where(p => productIds.Contains(p.Id))
                     .ProjectTo<ProductDto>(mapper.ConfigurationProvider).ToListAsync();
            }
            else
            {
                return new List<ProductDto>();
            }
        }

        public async Task<bool> InsertOrder(OrderBM order, int userId)
        {
            var productsFromDb = await GetProductsByIds(order.Products.Select(p => p.Id).ToArray());
            using (var scope = new TransactionScope())
            {
                try
                {

                    var invoice = new Invoice
                    {
                        BuyerId = userId,
                        IssuedOn = DateTime.UtcNow
                    };

                    var savedInvoice = context.Invoice.Add(invoice);
                    context.SaveChanges();

                    foreach (var item in order.Products)
                    {
                        var newInvoiceItem = new InvoiceProduct
                        {
                            InvoiceId = savedInvoice.Entity.Id,
                            ProductId = item.Id,
                            Quantity = item.Amount,
                            Price = productsFromDb.FirstOrDefault(p => p.Id == item.Id).Price
                        };
                        context.InvoiceProduct.Add(newInvoiceItem);
                    }

                    context.SaveChanges();
                    scope.Complete();
                    return true;

                }
                catch (Exception ex)
                {
                    throw new CustomAppException(ex.Message);
                }
            }

        }
    }
}
